package assignment567;

import java.util.*;

public class Mobile extends Bill implements IDisplay {
//    Mobile Manufacturer Name, Plan Name, mobile number, Internet GB used, Minute used
	private String manufacturer_Name;
	private String plan_Name;
	private int mobile_Number;
	private Double internet_GB_USED;
	private double minute_used;

	public Mobile(int bill_id, Date bill_date, String bill_type, double bill_amount, String manufacturerName,
			String planName, int mbNumber, Double internetuse, double minuteUsed) {
		super(bill_id, bill_date, bill_type, bill_amount);
		this.manufacturer_Name = manufacturerName;
		this.plan_Name = planName;
		this.mobile_Number = mbNumber;
		this.internet_GB_USED = internetuse;
		this.minute_used = minuteUsed;
	}

	public String getManufacturer_Name() {
		return manufacturer_Name;
	}

	public void setManufacturer_Name(String manufacturer_Name) {
		this.manufacturer_Name = manufacturer_Name;
	}

	public String getPlan_Name() {
		return plan_Name;
	}

	public void setPlan_Name(String plan_Name) {
		this.plan_Name = plan_Name;
	}

	public int getMobile_Number() {
		return mobile_Number;
	}

	public void setMobile_Number(int mobile_Number) {
		this.mobile_Number = mobile_Number;
	}

	public Double getInternet_GB_USED() {
		return internet_GB_USED;
	}

	public void setInternet_GB_USED(Double internet_GB_USED) {
		this.internet_GB_USED = internet_GB_USED;
	}

	public double getMinute_used() {
		return minute_used;
	}

	public void setMinute_used(double minute_used) {
		this.minute_used = minute_used;
	}

	@Override
	public void display() {
		System.out.println("Bill Id :" +getBill_id());
		System.out.println("Bill Date : "+getBill_date());
		System.out.println("Bill Type :"+getBill_type());
		System.out.println("Bill Amount : $"+getTotal_bill_amount());
		System.out.println("Manufacturer Name :"+getManufacturer_Name());
		System.out.println("Plane Name :"+ getPlan_Name());
		System.out.println("Mobile Number :"+getMobile_Number());
		System.out.println("Internet Usage :"+ getInternet_GB_USED()+"GB");
		System.out.println("Minutes Usage : "+getMinute_used()+"minutes");
		System.out.println("*******************************************");

		
	}

}
