package assignment567;

import java.util.*;

public class Main {

	public static void main(String[] args)
	{
		Bill b = new Hydro(1, new Date(), "Hydro" , 200.00, "HydroOne", 230);
		Bill b1 = new Internet(2, new Date(), "Internet" , 50.00, "Rogers", 5.0) ;
		Bill b3 = new Mobile(3, new Date(), "Mobile", 45.5, "Bell","Talk+Text", 4379901 ,2.5, 80.0);
        
		//		Ist customer who has 3 bills
		ArrayList<Bill> bill1 = new ArrayList<>();
		bill1.add(b);
		bill1.add(b1);
		bill1.add(b3);
	
		Customer c1 = new Customer(2, "Raman", "kaur", "Ramanpreet Kaur", "raman95@yahoo.com", bill1 );
		c1.setBillList(bill1);
       
       
        ArrayList<Bill> bill2 = new ArrayList<>();
        bill2.add(b);
        bill2.add(b1);
        Customer c2= new Customer(3, "Param", "kaur", "Paramjeet Kaur", "param@yahoo.com", bill2 );
        c2.setBillList(bill2);
          
        ArrayList<Bill> bill3 = new ArrayList<>();
        Customer c3= new Customer(1, "gagan", "kaur", "gaganjeet Kaur", "gagan@yahoo.com", bill3 );
        c3.setBillList(bill3);
		CustomerGroup customergroup = new CustomerGroup();
		customergroup.addCustomer(c1);
		customergroup.addCustomer(c2);
		customergroup.addCustomer(c3);
		
		customergroup.display(); 
	  
	
	}

}
