package assignment567;

import java.util.*;

public class Internet extends Bill implements IDisplay {
	private String providerName;
	private Double internet_GB_used;

	public Internet(int bill_id, Date bill_date, String bill_type, double bill_amount, String providername,
			Double internet_use) {
		super(bill_id, bill_date, bill_type, bill_amount);
		this.providerName = providername;
		this.internet_GB_used = internet_use;

	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Double getInternet_GB_used() {
		return internet_GB_used;
	}

	public void setInternet_GB_used(Double internet_GB_used) {
		this.internet_GB_used = internet_GB_used;
	}

	@Override
	public void display() {
		
		System.out.println("Bill Id :"+ getBill_id() );
		System.out.println("Bill Date :"+ getBill_date());
	    System.out.println ("Bill Type : "+getBill_type());
	    System.out.println(" Amount :" + getTotal_bill_amount());
		System.out.println("Provider Name :"+providerName);
		System.out.println("*******************************************");

	}

}
