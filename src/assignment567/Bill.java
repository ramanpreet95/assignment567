package assignment567;

import java.util.*;

public class Bill implements IDisplay {
//	properties
	private int bill_id;
	private Date bill_date;
	private String bill_type;
	private Double total_bill_amount;

//   constructor 
	public Bill(int bill_id, Date bill_date, String bill_type, double bill_amount) {
		this.bill_id = bill_id;
		this.bill_date = bill_date;
		this.bill_type = bill_type;
		this.total_bill_amount = bill_amount;
	}
//   getter setter

	public int getBill_id() {
		return bill_id;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	public String getBill_type() {
		return bill_type;
	}

	public void setBill_type(String bill_type) {
		this.bill_type = bill_type;
	}

	public Double getTotal_bill_amount() {
		return total_bill_amount;
	}

	public void setTotal_bill_amount(Double total_bill_amount) {
		this.total_bill_amount = total_bill_amount;
	}

	public Date getBill_date() {
		return bill_date;
	}

	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}

	@Override
	public void display() {
		System.out.println("hello");
	}

}
