package assignment567;

import java.util.*;

public class Customer implements IDisplay, Comparable<Customer> {
//	Customer Id, first name, last name, full name, email id, 
//	Bill List  to store all types of bills and Total amount to pay
	private int customer_id;
	private String firstName;
	private String lastName;
	private String fullName;
	private String email_id;
	private ArrayList<Bill> billList = new ArrayList<>();



	public Customer(int customerId, String firstname, String lastname, String fullName, String emailId,
			ArrayList<Bill> billListl) {
		this.customer_id = customerId;
		this.firstName = firstname;
		this.lastName = lastname;
		this.fullName = fullName;
		this.email_id = emailId;
		this.billList = billListl;


	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public ArrayList<Bill> getBillList() {
		return billList;
	}

	public void setBillList(ArrayList<Bill> billList) {
		this.billList = billList;
	}
	
	
    
	@Override
	public void display() {
      
	
		System.out.println("Customer Id :" + customer_id);
		System.out.println("Customer FullName :" + fullName);
		System.out.println("Customer email ID :" + email_id);
		System.out.println("Bill Information");
		System.out.println("********************************");
		// TODO Rest
		double tot = 0;
		for (int i = 0; i < billList.size(); i++) {
			tot += billList.get(i).getTotal_bill_amount();
			billList.get(i).display();
		}
		
		 System.out.println("Total Bill amount to pay "+"$"+tot);
		 System.out.println("********************************");
	}
	@Override
	public int compareTo(Customer o) {
        if(this.getCustomer_id() < o.getCustomer_id()) {
        	return -1;
        } else if(this.getCustomer_id() > o.getCustomer_id()) {
        	return 1;
        } else {
        	return 0;
        }
    }
}
















