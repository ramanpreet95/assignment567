package assignment567;

import java.util.Date;

public class Hydro extends Bill implements IDisplay {
//	Agency Name, Unit consumed
	private String agencyName;
	private int unitConsumed;

	public Hydro(int bill_id, Date bill_date, String bill_type, double bill_amount, String agencyname,
			int unitConsumed) {
		super(bill_id, bill_date, bill_type, bill_amount);
		this.agencyName = agencyname;
		this.unitConsumed = unitConsumed;

	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public int getUnitConsumed() {
		return unitConsumed;
	}

	public void setUnitConsumed(int unitConsumed) {
		this.unitConsumed = unitConsumed;
	}

	@Override
	public void display()
	{
		System.out.println("Bill Id :"+ getBill_id());
		System.out.println("Bill Date :"+ getBill_date());
		System.out.println("Bill Type :"+ getBill_type());
		System.out.println("Bill Amount : $ "+ getTotal_bill_amount());
		System.out.println("Agency Name :"+getAgencyName());
		System.out.println("Unit Consumed :"+getUnitConsumed());
		System.out.println("***********************************************");

		
	}

}
